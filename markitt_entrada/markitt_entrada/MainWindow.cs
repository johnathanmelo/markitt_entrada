﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Proshot.CommandClient;

namespace markitt_entrada
{
    public partial class MainWindow : Form
    {
        string incomingMessage; // Lista de OCs: "numero_oc,nome_favorecido,quantidade_sacas,status_oc,peso_inicial|..."
        List<string> ListOC;
        int status_oc = -1;
        string codigo_oc;

        // Communication
        private CMDClient client;
        IPAddress ipServer = IPAddress.Parse("127.0.0.1");

        public MainWindow()
        {
            InitializeComponent();
            StartLogin();
        }

        private void StartLogin()
        {
            this.client = new CMDClient(ipServer, 8000, "Tela de Entrada");
            this.client.CommandReceived += new CommandReceivedEventHandler(client_CommandReceived);

            // Start timerLogin
            timerLogin.Enabled = true;
            timerLogin.Interval = 5000;
            timerLogin.Tick += new EventHandler(this.timerLogin_Tick);
        }

        void client_CommandReceived(object sender, CommandEventArgs e)
        {
            switch (e.Command.CommandType)
            {
                case (Proshot.CommandClient.CommandType.EntradaRequisitaListaOCs):
                    this.timerLogin.Enabled = false;
                    incomingMessage = e.Command.MetaData;
                    FillListOC();
                    FillComboBox();
                    break;
            }
        }

        private void FillListOC()
        {
            ListOC = incomingMessage.Split('|').ToList();
            ListOC.Remove(ListOC.Last());
        }

        private void FillComboBox()
        {
            comboBoxOcList.Text = "";
            comboBoxOcList.Items.Clear();

            foreach (string oc in ListOC)
            {
                List<string> OcInfo = oc.Split(',').ToList();
                string comboItem = OcInfo[0] + " - " + OcInfo[1].ToUpper(); // ex: "43012 - JOAO DA SILVA"
                comboBoxOcList.Items.Add(comboItem);
            }

            comboBoxOcList.Enabled = true;
            buttonRefresh.Enabled = true;
            buttonSend.Enabled = true;
        }

        private void timerLogin_Tick(object sender, EventArgs e)
        {
            if (this.client.Connected == false)
            {
                comboBoxOcList.Text = "Conectando...";
                comboBoxOcList.Enabled = false;
                buttonRefresh.Enabled = false;
                textBoxPesoInicial.ReadOnly = true;
                textBoxPesoFinal.ReadOnly = true;
                buttonSend.Enabled = false;

                this.client.ConnectToServer();
            }
            else
            {
                // envia requisição de lista de clientes para servidor
                this.client.SendCommand(new Command(Proshot.CommandClient.CommandType.EntradaRequisitaListaOCs, ipServer, null));
            }
        }

        private void buttonSend_Click(object sender, EventArgs e)
        {            
            if ( (status_oc == 0 && textBoxPesoInicial.Text != "") 
                || (status_oc == 1 && textBoxPesoFinal.Text != "")) // checa se os campos necessários foram preenchidos
            {

                if (this.client.Connected)
                {
                    // monta pacote de dados
                    string data = BuildDataPack();

                    // envia dados para servidor
                    this.client.SendCommand(new Command(Proshot.CommandClient.CommandType.EntradaOcInfo, ipServer, data));

                    // envia requisição de lista de clientes para servidor
                    this.client.SendCommand(new Command(Proshot.CommandClient.CommandType.EntradaRequisitaListaOCs, ipServer, null));

                    // reseta campos
                    comboBoxOcList.Text = "";
                    status_oc = -1;

                    // mensagem informativa
                    MessageBox.Show("OC enviada!");
                }
                else
                {
                    this.timerLogin.Enabled = true;
                }
            }
        }

        private string BuildDataPack()
        {
            string data; // "codigo_oc,status_oc,peso"

            data = codigo_oc + ',' + status_oc.ToString() + ',';

            if (status_oc == 0)
            {
                data = data + textBoxPesoInicial.Text;
            }
            else if (status_oc == 1)
            {
                data = data + textBoxPesoFinal.Text;
            }

            return data;
        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            ClearOcInfoFields();

            if (this.client.Connected == true)
            {
                // envia requisição de lista de OCs para servidor
                this.client.SendCommand(new Command(Proshot.CommandClient.CommandType.EntradaRequisitaListaOCs, ipServer, null));
            }
            else
            {
                this.timerLogin.Enabled = true;
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            this.client.Disconnect();
        }

        private void comboBoxOcList_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearOcInfoFields();
            int idx = comboBoxOcList.SelectedIndex;
            string selected_oc = ListOC[idx];
            FillOcInfo(selected_oc);
        }        

        private void FillOcInfo(string oc)
        {
            List<string> OcInfo = oc.Split(',').ToList();
            codigo_oc = OcInfo[0];
            textBoxCodigo.Text = codigo_oc;
            textBoxFavorecido.Text = OcInfo[1].ToUpper();
            int quantidade_sacas = Int32.Parse(OcInfo[2]);
            status_oc = Int32.Parse(OcInfo[3]);            

            if (status_oc == 0) // caminhao vai para primeira pesagem
            {                
                textBoxPesoInicial.ReadOnly = false;                
                textBoxPesoFinal.ReadOnly = true;
            }
            else if (status_oc == 1) // caminhao vai para segunda pesagem
            {
                textBoxPesoInicial.ReadOnly = true;
                textBoxPesoInicial.Text = OcInfo[4];
                                
                textBoxPesoFinal.ReadOnly = false;
            }
        }

        private void ClearOcInfoFields()
        {
            textBoxCodigo.Clear();
            textBoxFavorecido.Clear();
            textBoxPesoInicial.Clear();
            textBoxPesoFinal.Clear();
            textBoxPesoInicial.ReadOnly = true;
            textBoxPesoFinal.ReadOnly = true;
        }

        private void textBoxPesoInicial_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void textBoxPesoFinal_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
    }
}
