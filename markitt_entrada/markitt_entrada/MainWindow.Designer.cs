﻿namespace markitt_entrada
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.comboBoxOcList = new System.Windows.Forms.ComboBox();
            this.labelOcList = new System.Windows.Forms.Label();
            this.buttonSend = new System.Windows.Forms.Button();
            this.timerLogin = new System.Windows.Forms.Timer(this.components);
            this.buttonRefresh = new System.Windows.Forms.Button();
            this.labelOcInfo = new System.Windows.Forms.Label();
            this.labelFavorecido = new System.Windows.Forms.Label();
            this.labelCodigo = new System.Windows.Forms.Label();
            this.labelPesoInicial = new System.Windows.Forms.Label();
            this.labelPesoFinal = new System.Windows.Forms.Label();
            this.textBoxFavorecido = new System.Windows.Forms.TextBox();
            this.textBoxCodigo = new System.Windows.Forms.TextBox();
            this.textBoxPesoInicial = new System.Windows.Forms.TextBox();
            this.textBoxPesoFinal = new System.Windows.Forms.TextBox();
            this.labelUnidade = new System.Windows.Forms.Label();
            this.labelUnidade2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // comboBoxOcList
            // 
            this.comboBoxOcList.Enabled = false;
            this.comboBoxOcList.FormattingEnabled = true;
            this.comboBoxOcList.Location = new System.Drawing.Point(12, 31);
            this.comboBoxOcList.Name = "comboBoxOcList";
            this.comboBoxOcList.Size = new System.Drawing.Size(402, 21);
            this.comboBoxOcList.TabIndex = 0;
            this.comboBoxOcList.Text = "Conectando...";
            this.comboBoxOcList.SelectedIndexChanged += new System.EventHandler(this.comboBoxOcList_SelectedIndexChanged);
            // 
            // labelOcList
            // 
            this.labelOcList.AutoSize = true;
            this.labelOcList.Location = new System.Drawing.Point(9, 15);
            this.labelOcList.Name = "labelOcList";
            this.labelOcList.Size = new System.Drawing.Size(98, 13);
            this.labelOcList.TabIndex = 1;
            this.labelOcList.Text = "Selecione uma OC:";
            // 
            // buttonSend
            // 
            this.buttonSend.Enabled = false;
            this.buttonSend.Location = new System.Drawing.Point(179, 217);
            this.buttonSend.Name = "buttonSend";
            this.buttonSend.Size = new System.Drawing.Size(102, 23);
            this.buttonSend.TabIndex = 6;
            this.buttonSend.Text = "Enviar";
            this.buttonSend.UseVisualStyleBackColor = true;
            this.buttonSend.Click += new System.EventHandler(this.buttonSend_Click);
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.BackgroundImage = global::markitt_entrada.Properties.Resources.refresh;
            this.buttonRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonRefresh.Enabled = false;
            this.buttonRefresh.Location = new System.Drawing.Point(427, 26);
            this.buttonRefresh.Name = "buttonRefresh";
            this.buttonRefresh.Size = new System.Drawing.Size(30, 30);
            this.buttonRefresh.TabIndex = 7;
            this.buttonRefresh.UseVisualStyleBackColor = true;
            this.buttonRefresh.Click += new System.EventHandler(this.buttonRefresh_Click);
            // 
            // labelOcInfo
            // 
            this.labelOcInfo.AutoSize = true;
            this.labelOcInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOcInfo.Location = new System.Drawing.Point(9, 74);
            this.labelOcInfo.Name = "labelOcInfo";
            this.labelOcInfo.Size = new System.Drawing.Size(119, 13);
            this.labelOcInfo.TabIndex = 8;
            this.labelOcInfo.Text = "Informações da OC:";
            // 
            // labelFavorecido
            // 
            this.labelFavorecido.AutoSize = true;
            this.labelFavorecido.Location = new System.Drawing.Point(144, 103);
            this.labelFavorecido.Name = "labelFavorecido";
            this.labelFavorecido.Size = new System.Drawing.Size(63, 13);
            this.labelFavorecido.TabIndex = 9;
            this.labelFavorecido.Text = "Favorecido:";
            // 
            // labelCodigo
            // 
            this.labelCodigo.AutoSize = true;
            this.labelCodigo.Location = new System.Drawing.Point(9, 103);
            this.labelCodigo.Name = "labelCodigo";
            this.labelCodigo.Size = new System.Drawing.Size(43, 13);
            this.labelCodigo.TabIndex = 10;
            this.labelCodigo.Text = "Código:";
            // 
            // labelPesoInicial
            // 
            this.labelPesoInicial.AutoSize = true;
            this.labelPesoInicial.Location = new System.Drawing.Point(12, 140);
            this.labelPesoInicial.Name = "labelPesoInicial";
            this.labelPesoInicial.Size = new System.Drawing.Size(64, 13);
            this.labelPesoInicial.TabIndex = 11;
            this.labelPesoInicial.Text = "Peso Inicial:";            
            // 
            // labelPesoFinal
            // 
            this.labelPesoFinal.AutoSize = true;
            this.labelPesoFinal.Location = new System.Drawing.Point(210, 140);
            this.labelPesoFinal.Name = "labelPesoFinal";
            this.labelPesoFinal.Size = new System.Drawing.Size(59, 13);
            this.labelPesoFinal.TabIndex = 12;
            this.labelPesoFinal.Text = "Peso Final:";
            // 
            // textBoxFavorecido
            // 
            this.textBoxFavorecido.Location = new System.Drawing.Point(213, 100);
            this.textBoxFavorecido.Name = "textBoxFavorecido";
            this.textBoxFavorecido.ReadOnly = true;
            this.textBoxFavorecido.Size = new System.Drawing.Size(244, 20);
            this.textBoxFavorecido.TabIndex = 13;
            // 
            // textBoxCodigo
            // 
            this.textBoxCodigo.Location = new System.Drawing.Point(57, 100);
            this.textBoxCodigo.Name = "textBoxCodigo";
            this.textBoxCodigo.ReadOnly = true;
            this.textBoxCodigo.Size = new System.Drawing.Size(81, 20);
            this.textBoxCodigo.TabIndex = 14;
            // 
            // textBoxPesoInicial
            // 
            this.textBoxPesoInicial.Location = new System.Drawing.Point(82, 137);
            this.textBoxPesoInicial.Name = "textBoxPesoInicial";
            this.textBoxPesoInicial.ReadOnly = true;
            this.textBoxPesoInicial.Size = new System.Drawing.Size(72, 20);
            this.textBoxPesoInicial.TabIndex = 15;
            this.textBoxPesoInicial.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxPesoInicial_KeyPress);
            // 
            // textBoxPesoFinal
            // 
            this.textBoxPesoFinal.Location = new System.Drawing.Point(275, 137);
            this.textBoxPesoFinal.Name = "textBoxPesoFinal";
            this.textBoxPesoFinal.ReadOnly = true;
            this.textBoxPesoFinal.Size = new System.Drawing.Size(72, 20);
            this.textBoxPesoFinal.TabIndex = 16;
            this.textBoxPesoFinal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxPesoFinal_KeyPress);
            // 
            // labelUnidade
            // 
            this.labelUnidade.AutoSize = true;
            this.labelUnidade.Location = new System.Drawing.Point(353, 140);
            this.labelUnidade.Name = "labelUnidade";
            this.labelUnidade.Size = new System.Drawing.Size(22, 13);
            this.labelUnidade.TabIndex = 17;
            this.labelUnidade.Text = "KG";
            // 
            // labelUnidade2
            // 
            this.labelUnidade2.AutoSize = true;
            this.labelUnidade2.Location = new System.Drawing.Point(160, 140);
            this.labelUnidade2.Name = "labelUnidade2";
            this.labelUnidade2.Size = new System.Drawing.Size(22, 13);
            this.labelUnidade2.TabIndex = 18;
            this.labelUnidade2.Text = "KG";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(470, 252);
            this.Controls.Add(this.labelUnidade2);
            this.Controls.Add(this.labelUnidade);
            this.Controls.Add(this.textBoxPesoFinal);
            this.Controls.Add(this.textBoxPesoInicial);
            this.Controls.Add(this.textBoxCodigo);
            this.Controls.Add(this.textBoxFavorecido);
            this.Controls.Add(this.labelPesoFinal);
            this.Controls.Add(this.labelPesoInicial);
            this.Controls.Add(this.labelCodigo);
            this.Controls.Add(this.labelFavorecido);
            this.Controls.Add(this.labelOcInfo);
            this.Controls.Add(this.buttonRefresh);
            this.Controls.Add(this.buttonSend);
            this.Controls.Add(this.labelOcList);
            this.Controls.Add(this.comboBoxOcList);
            this.Name = "MainWindow";
            this.Text = "Entrada";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxOcList;
        private System.Windows.Forms.Label labelOcList;
        private System.Windows.Forms.Button buttonSend;
        private System.Windows.Forms.Timer timerLogin;
        private System.Windows.Forms.Button buttonRefresh;
        private System.Windows.Forms.Label labelOcInfo;
        private System.Windows.Forms.Label labelFavorecido;
        private System.Windows.Forms.Label labelCodigo;
        private System.Windows.Forms.Label labelPesoInicial;
        private System.Windows.Forms.Label labelPesoFinal;
        private System.Windows.Forms.TextBox textBoxFavorecido;
        private System.Windows.Forms.TextBox textBoxCodigo;
        private System.Windows.Forms.TextBox textBoxPesoInicial;
        private System.Windows.Forms.TextBox textBoxPesoFinal;
        private System.Windows.Forms.Label labelUnidade;
        private System.Windows.Forms.Label labelUnidade2;
    }
}

